import Vue from 'vue'
import Vuex from 'vuex'
import clients from './clients/index'
import modal from './modal/index'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        clients: clients,
        modal: modal
    }
})